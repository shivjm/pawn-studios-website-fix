<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "About";
$page_location = "about.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
    <?php require('src/elements/header.php') ?>

    <?php require('src/elements/navigation.php') ?>

    <table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr valign="top">
            <td id="maincontent" width="850">
                <h2>About Pawn</h2>
                <p>
                    PawnGame.com was officially launched in August 2006 with two main developers (Blank101 and JPillz).
                    Since then, they made it their goal to provide an interactive arena for users to play and discuss
                    multiplayer Flash games. The games released here at Pawn Game are absolutely free of charge for
                    everyone.
                </p>

                <p>
                    Their first project, Pawn, was publicly launched in May 2007. Since then, it has seen over 10
                    million visitors worldwide. Due to the high reception of Pawn, work soon moved to a sequel - Pawn:
                    Tactics. A year later in June 2008, Pawn Tactics became open for public beta testing. The game was
                    officially launched in March 2009.
                </p>

                <p>
                    Following six successful years, PawnGame.com took a rest and Pawn Tactics moved to SandBoxd.
                    SandBoxd was a gaming portal founded by Blank101 and JPillz that supported all devices with a web
                    browser.
                    Pawn Tactics was one of the few games on the website that required Flash Player and would not work
                    on mobile devices.
                    Pawn was eventually brought to SandBoxd and both games benefited from some game updates developed by
                    community members.
                </p>

                <p>
                    By 2019, SandBoxd was ready to be retired. Through the remaining Pawn community members' dedication,
                    PawnGame.com was brought back in to existence along with a brand-new forum. The games were migrated
                    and now enjoy being the focus of the website.
                </p>

                <p>
                    Thank you to our entire community for being here with us all these years, to returning members,
                    and to new members here to experience what Pawn is all about!
                    We hope PawnGame.com will be your home for years to come. :)
                </p>

                <p></p>

                <h2>What Makes Our Games So Unique?</h2>

                <ul>
                    <li>
                        Our games have always been completely free to play.
                        This now includes being free from advertisements.
                    </li>
                    <br>
                    <li>Pawn is dedicated to being a safe gaming environment. We have a strict policy on swearing and racism so that our games
                        can be available to many different age groups.
                    </li>
                    <br>
                    <li>Our games have been around for over 10 years and continue to receive support.</li>
                    <br>
                    <li>Our friendly staff are here to provide you with any assistance you need.</li>
                    <br>
                </ul>
            </td>
            <td width="185" valign="top" align="center">
            </td>
        </tr>
        </tbody>
    </table>

    <?php require('src/elements/footer.php') ?>

</body>
</html>