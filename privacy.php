<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "Privacy Policy";
$page_location = "privacy.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
<?php require('src/elements/header.php') ?>

<?php require('src/elements/navigation.php') ?>

<table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
    <tr valign="top">
        <td id="maincontent" width="850">
            Introduction

            At PawnGame.com we recognize your right to privacy on the internet.
            Starting our 2019 relaunch we have never served advertisements or embedded analytical tracking into our website.
            We will always strive to ensure your relationship with PawnGame.com stays on PawnGame.com and is not shared with any third parties.
            Our website has been configured to force viewers to use an encrypted connection at all times.

            Outlined below is our collection and use of your data. We will do our best to ensure this data is accurate and kept up-to-date.


            Website

            Data We Deliberately Collect and Store | Who Has Access to this Data   | Details

            Username							  |   Everyone                    |    This is your public forum identifier.
            Password                              |   No one                      |    Passwords are always hashed before they are stored.
            Email Address                         |   Administrators              |    Our forum software provides access to emails any time a user's properties are edited, such as user groups. Emails will only be used by administrators for account verification and site-wide announcements. Our forum software may send you emails for verification and notification purposes based on your account settings.
            IP Address                            |   Administrators              |    IP addresses may occassionally be checked to keep our website safe. Such checks include looking up the IP address details in a public database to determine if the connection is from a spam source. Administrators are not allowed to share with anyone the geographical region your IP address belongs to.
            Forum Submitted Content               |   Everyone, Administrators    |    Most forum content except for private conversations are published publicly, or to their corresponding user group. Administrators may access private conversations to keep our website safe.

            Data Stored on Your Device | Details

            Session tokens             | When you login to our website, a token is generated which is sent to your device to be stored into a cookie. When visiting our website this token is sent back to the website to either present the page as a logged in user or guest.


            Other Data Stored by Software We Use  | Who Has Access to this Data   | Details

            User Agent and Requested Page and the Result | Project Manager        |  Our website software automatically logs all page requests. These are used for security purposes to detect attacks on our website, as well as understanding and correcting errors in our website.

            Indirect Tracking on our Forum by Third Party Media Providers

            Because our forum allows users to embed media in their posts such as videos and gifs, when visiting certain pages of the forum your web browser may request content from third party media providers.
            Web browsers generally send a "Referer" header when requesting content from a websites, therefore these third party media providers will be able to discover you are visiting PawnGame.com.

            Games

            Pawn

            Data We Deliberately Collect and Store | Who Has Access to this Data   | Details

            IP Address                            | Administrators
            Chats                                 | Administrators                | To help keep our game safe administrators may from time to time monitor the chat.
            Game Progress                         | Everyone                      | Your game progress is saved to your account and published publicly on our leaderboards with your username.
            User Submitted Maps                   | Administrators, Everyone      | Administrators have access to all user maps. Your maps are private until you host a match, at which point anyone playing may be capable of taking your map design.

            Pawn Tactics

            Data We Deliberately Collect and Store | Who Has Access to this Data   | Details

            IP Address                            | Administrators
            Game Progress                         | Everyone                      | Your game progress is saved to your account and published publicly on our leaderboards with your username. Other game progress may be stored internally and published in the future or used to identify and mitigate cheating.

            When is data aggregated and who is it used by?

            When investigating breaches of terms of use such as game or account hacking, collected data may be aggregated by the staff investigating the incident. The staff will only have access to the data their role permits. For example, a game moderator will never have access to website requested page logs or email addresses.

            Due to the nature of their access, the Project Manager is the only one capable of accessing any data stored or transmited by the services and may do so, especially to keep the services secure and functioning.

            Use of Third Party Providers

            We use AWS (Amazon Web Services) to keep our services online. Your data is stored on AWS servers. Please see their privacy policy to learn how they keep our data secure.
            We use Discord for staff communication, as well as publishing game progress. Some game collected game data is stored on Discord in the form of logs. Please see their privacy policy to learn how they keep chats private.

            Links:

            Staff list.
        <td width="185" valign="top" align="center">
        </td>
    </tr>
    </tbody>
</table>

<?php require('src/elements/footer.php') ?>

</body>
</html>