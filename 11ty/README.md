PawnGame.com Website
====================

Written in HTML, JS, CSS, and 11ty.

### Licensing
Most of the source code in this project is licensed under MPL 2.0.   
See individual file headers for details.   

See the file [LICENSE](./LICENSE) for full details about the Mozilla Public License.