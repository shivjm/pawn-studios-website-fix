/*
 * Copyright (c) 2023  Alex (Pawn Studios)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

const pluginWebc = require("@11ty/eleventy-plugin-webc");
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const yaml = require("js-yaml");

module.exports = function(eleventyConfig) {
    eleventyConfig.addPlugin(pluginWebc, {
        components: "src/_components/**/*.webc"
    });
    eleventyConfig.addPlugin(eleventyNavigationPlugin);
    eleventyConfig.addDataExtension("yml", contents => yaml.safeLoad(contents));
    eleventyConfig.addGlobalData("permalink", () => {
        let extension = ".html"
        return (data) => `${data.page.filePathStem}${extension}`;
    });
    eleventyConfig.addPassthroughCopy({
        "./images/": "/images/",
        "./css/": "/css/"
    });

    return {
        dir: {
            input: "src",
            data: "_data",
            includes: "_includes",
            layouts: "_layouts",
            output: "_site"
        },
        htmlTemplateEngine: "webc"
    }
};