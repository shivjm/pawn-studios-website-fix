---
# Copyright © 2023  Alex
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

---

<!DOCTYPE html>
<script webc:setup>
    function getTitle(pageRef) {
        let pageRefX = pageRef;
        return this.page_name ? this.global.site_title + " - " + this.page_name : this.global.site_title;
    }
    function getUrl() {
        return this.global.site_location + this.page.url;
    }
</script>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title @text="getTitle(page)"></title>

        <meta name="description" :content="global.site_description" />

        <meta property="og:description" :content="global.site_description" />
        <meta property="twitter:description" :content="global.site_description" />

        <meta property="og:site_name" :content="global.site_title" />
        <meta property="og:type" content="website" />
        <meta property="og:title" :content="getTitle()" />
        <meta property="twitter:title" :content="getTitle()" />
        <meta property="og:url" :content="getUrl()" />
        <meta property="og:image" content="https://d1h3njoxaty54c.cloudfront.net/website/pawn_icon.png" />
        <meta property="twitter:image" content="https://d1h3njoxaty54c.cloudfront.net/website/pawn_icon.png" />
        <meta property="twitter:card" content="summary" />

        <meta name="apple-mobile-web-app-title" content="Pawn Game Website">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <link rel="apple-touch-icon" href="https://d1h3njoxaty54c.cloudfront.net/website/pawn_icon_dark_red_2.png" />
        <link rel="canonical" :href="getUrl()" />
        <link rel="icon" type="image/png" href="https://d1h3njoxaty54c.cloudfront.net/website/pawn_favicon.png" sizes="32x32" />

        <link rel="stylesheet" href="/css/modern-normalize.css" webc:keep>
        <link rel="stylesheet" :href="getBundleFileUrl('css')" webc:keep>
        <script :src="getBundleFileUrl('js')" webc:keep></script>
    </head>

    <style>
        :root {
            --pawn-gold: #EDA200;
            --pawn-red: #FD0000;
            --heading-red: #D40D0D;
            --content-background: #2A2A2A;
            --content-border: #424242;
        }

        body {
            display: flex;
            color: #AAAAAA;
            font-family: Tahoma, Verdana, Geneva, Arial, Helvetica, sans-serif;
            font-size: clamp(0.8rem, calc(0.5vw + 0.5rem), 9001rem);
            background: #000000 url(/images/bg_main.gif) repeat-x top;
        }

        a {
            font-weight: bold;
            color: var(--pawn-gold);
            text-decoration: none;
        }

        a:hover, a:active {
            color: var(--pawn-red);
        }

        #site-container {
            display: flex;
            align-content: center;
            flex-grow: 2;
            flex-shrink: 1;
            flex-basis: max-content;
            flex-direction: column;
            min-width: 150px;
        }

        .container-padding {
            min-width: 15px;
            flex-grow: 60;
            flex-shrink: 2;
        }

        #main-content-container {
            background: var(--content-background);
            border: calc(0.1em + 1px) solid var(--content-border);
            border-top-color: #000000;
        }

        #main-content {
            margin-left: 2em;
        }

        #main-content p {
            font-size: .9em;
            line-height: 1.9em;
            margin: 0 2em 2.1em .8em;
        }

        #main-content h2 {
            font-size: 1.1em;
            line-height: 2em;
            color: var(--heading-red);
            border-bottom: 0.1em dashed var(--heading-red);
            margin: 1em 2em 1em -0.8em;
        }

        #main-content h3 {
            font-size: .9em;
            line-height: 1.9em;
            margin: 0 2em 0.7em .8em;
        }
    </style>

    <body>
        <remaining-space @attributes class="container-padding"></remaining-space>
        <div id="site-container">
            <banner></banner>
            <navigation></navigation>
            <div id="main-content-container">
                <div id="main-content">
                    <main @raw="content"></main>
                </div>
            </div>
            <footer-bar></footer-bar>
        </div>
        <remaining-space @attributes class="container-padding"></remaining-space>
    </body>
</html>
