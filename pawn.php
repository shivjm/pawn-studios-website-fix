<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "Pawn";
$page_location = "pawn.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
    <?php require('src/elements/header.php') ?>

    <?php require('src/elements/navigation.php') ?>

    <table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
            <tr valign="top">
                <td id="maincontent" width="650">
                    <script type="text/javascript">
                        function openGame() {
                            var popup = window.open("playpawn.php", "pawn", "width=" + screen.width + ", height=" + screen.height + ", scrollbars=no, resizable=yes, toolbar=no, location=no, status=no");
                            if (popup.outerWidth < screen.availWidth || popup.outerHeight < screen.availHeight)
                            {
                                popup.moveTo(0,0);
                                popup.resizeTo(screen.availWidth, screen.availHeight);
                            }
                        }
                    </script>


                    <h2>Pawn</h2>

                    <div align="center">
                        <a href="https://pawngame.com/forum/threads/adobe-flash-player-web-plugin-end-of-life.1484/"><img src="https://pawngame.com/static/website/landing/image/playnow.gif" border="0"></a><br>
                        <a href="https://pawngame.com/forum/pages/pawn/">Alternate Link (Web Flash Player)</a>
                    </div>

                    <h2>Controls</h2>
                    <p>Movement</p>
                    <ul>
                        <li>W/Up - Climb ladder</li>
                        <li>S/Down - Crouch/Climb down ladder</li>
                        <li>A/Left - Walk left</li>
                        <li>D/Right - Walk right</li>
                        <li>Space/Ctrl - Jump</li>
                    </ul>
                    <p>Weapons</p>
                    <ul>
                        <li>0-9 - Select weapon (Press again to select secondary weapon)</li>
                        <li>Left Click - Fire weapon</li>
                        <li>Scroll Wheel - Cycle weapons</li>
                    </ul>
                    <p>Chat</p>
                    <ul>
                        <li>K - Send a public chat message</li>
                        <li>L - Send a team chat message</li>
                        <li>Enter - Send the message</li>
                    </ul>
                    <p>Other</p>
                    <ul>
                        <li>E/Numpad 0 - Purchase ammo/health refill from ammo crate (Costs 3 kill coins)</li>
                        <li>Shift - Hold this key down to display the game scores</li>
                    </ul>

                    <h2>Links</h2>
                    <ul>
                        <li><a href="https://www.pawngame.com/rank.php">Rank</a></li>
                        <li><a href="https://www.pawngame.com/ranklist.php">Rank List</a></li>
                        <li><a href="https://pawngame.com/forum/forums/pawn-updates.85/">Changelog</a></li>
                        <li><a href="https://discord.gg/XRyut9Z" target="_blank">Join us on Discord!</a></li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>

    <?php require('src/elements/footer.php') ?>
</body>
</html>