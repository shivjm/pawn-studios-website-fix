<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "Games";
$page_location = "games.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
 <?php require('src/elements/header.php') ?>

 <?php require('src/elements/navigation.php') ?>

    <table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
        <tr valign="top">
            <td id="maincontent" width="850">
                <h2>Our Games</h2>
                <div align="center">
                    <table width="90%" border="0">
                        <tbody>
                        <tr>
                            <td class="gamethumbnails"><a
                                    href="https://pawngame.com/forum/threads/download-pawn.1592/">
                                <img src="https://pawngame.com/static/website/landing/image/pawn.png" border="2"></a><br><br>
                            </td>
                            <td align="left"><p>Pawn - Jump into a deathmatch with other players from all over the world!
                                Play on a pre-built map or create your own.<br>
                                    <a href="https://pawngame.com/forum/threads/november-2022-status-update.1633/">Offline</a>
                <?php /*            <a href="https://d1h3njoxaty54c.cloudfront.net/pawn/Pawn.exe">Download</a> <b>·</b> <a href="https://pawngame.com/forum/threads/download-pawn.1592/">Info</a> <b>·</b> <a href="https://pawngame.com/forum/forums/pawn-updates.85/">Release Notes</a> */ ?>
                            </p></td>
                        </tr>

                        <tr>
                            <td class="gamethumbnails"><a
                                    href="https://discord.gg/6U2p6Gs">
                                <img src="https://pawngame.com/static/website/landing/image/pawntactics.png" border="2"></a><br><br>
                            </td>
                            <td align="left"><p>Pawn: Tactics - We've taken a different approach to
                                the Pawn series on this one. Featuring 8 playable classes, stat
                                tracking, clans and much more.<br>
                                    <a href="https://pawngame.com/forum/threads/november-2022-status-update.1633/">Offline</a>
<?php /*                            <a href="https://d1h3njoxaty54c.cloudfront.net/pawn-tactics/Pawn%20Tactics.exe">Download</a> <b>·</b> <a href="https://pawngame.com/forum/threads/download-pawn-tactics.1471/">Info</a> <b>·</b> <a href="https://pawngame.com/forum/forums/pawn-tactics-updates.86/">Release Notes</a> */ ?>
                            </p></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <h2>FAQ</h2>
                <div align="center">
                    <table width="90%" border="0">
                        <tbody>
                        <tr>
                            <td align="left">
                                <h3>Question: How do I register an account?</h3>
                                <p>Answer: Once registered on our <a href="https://pawngame.com/forum/register/">forum</a>, you can login to the games using your forum name and password.</p>
                            <td align="left">
                        </tr>
                        </tbody>
                    </table>
                </div>
            </td>
            <td width="185" valign="top" align="center">
            </td>
        </tr>
        </tbody>
    </table>

    <?php require('src/elements/footer.php') ?>

</body>
</html>