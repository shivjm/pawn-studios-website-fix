<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

require_once ('src/xf/Finder.php');
require_once ('src/xf/UserStatus.php');
require_once ('src/pawn/Rankings.php');

use PawnStudios\Finder;
use PawnStudios\Rankings;
use PawnStudios\UserStatus;

$rankings_start = 0;
$rankings_end = 100;
$rankings_cache_key = 'pawn-rankings';
$rankings_cache_ttl_seconds = 60;

global $page_name;
global $page_location;
$page_name = "Pawn Rankings";
$page_location = "rank.php";
?>

<?php
$rankings = array();

if (apcu_exists($rankings_cache_key)) {
    $rankings = apcu_fetch($rankings_cache_key);
} else {
    $rankings = Rankings::getRankingSet($rankings_start, $rankings_end);
    apcu_store($rankings_cache_key, $rankings, $rankings_cache_ttl_seconds);
}

$finder = new Finder();

foreach ($rankings as $ranking) {
    $user = $finder->findUserFromName($ranking->name);

    $ranking->is_moderator = isset($user) ? UserStatus::is_moderator($user) : false;
    $ranking->is_admin = isset($user) ? UserStatus::is_admin($user) : false;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>
</head>

<body>
    <?php require('src/elements/header.php') ?>

    <?php require('src/elements/navigation.php') ?>

    <table width="1035" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
            <tr valign="top">
                <td id="maincontent" width="650">
                    <h2>Pawn Rankings</h2>
                    <div align="center">
                        <table width="90%" cellspacing="3" cellpadding="3">
                            <tbody id="ranking-table">
                                <tr>
                                    <th>Rank:</th>
                                    <th>Name:</th>
                                    <th>Score:</th>
                                    <th>Kills:</th>
                                    <th>Deaths:</th>
                                    <th>MVP:</th>
                                    <th>K/D:</th>
                                    <th>Avatar:</th>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <?php require('src/elements/footer.php') ?>
    <script>
        function createIconElement(alt, src) {
            let icon = document.createElement("img");
            icon.alt = alt;
            icon.src = src;
            return icon;
        }

        function createRankingRowElement(ranking) {
            let rankingRow = document.createElement("tr");
            let rankingColumnNum = document.createElement("td");
            let rankingColumnName = document.createElement("td");
            let rankingColumnScore = document.createElement("td");
            let rankingColumnKills = document.createElement("td");
            let rankingColumnDeaths = document.createElement("td");
            let rankingColumnMVP = document.createElement("td");
            let rankingColumnKD = document.createElement("td");
            let rankingColumnIcons = document.createElement("td");

            rankingRow.appendChild(rankingColumnNum);
            rankingRow.appendChild(rankingColumnName);
            rankingRow.appendChild(rankingColumnScore);
            rankingRow.appendChild(rankingColumnKills);
            rankingRow.appendChild(rankingColumnDeaths);
            rankingRow.appendChild(rankingColumnMVP);
            rankingRow.appendChild(rankingColumnKD);
            rankingRow.appendChild(rankingColumnIcons);

            rankingColumnNum.innerText = ranking.ranking;
            rankingColumnName.innerText =  ranking.name;
            rankingColumnScore.innerText = ranking.score;
            rankingColumnKills.innerText =  ranking.kills;
            rankingColumnDeaths.innerText = ranking.deaths;
            rankingColumnMVP.innerText =  ranking.mvp;
            rankingColumnKD.innerText =  ranking.kd;

            let rankIconsLocation = "https://pawngame.com/static/website/landing/image/ranks/";
            let placeHolderIconName = "placeholder.png";
            let eliteIcon = createIconElement("", rankIconsLocation + placeHolderIconName);
            let rankIcon = createIconElement(ranking.rank.rank_title, rankIconsLocation + ranking.rank.rank_image);
            let staffIcon = createIconElement("", rankIconsLocation + placeHolderIconName);

            if (ranking.ranking <= 10) {
                eliteIcon = createIconElement("Pawn Elite", rankIconsLocation + "pawn.png");
            }
            if (ranking.is_admin) {
                staffIcon = createIconElement("Admin", rankIconsLocation + "admin.png");
            } else if (ranking.is_moderator) {
                staffIcon = createIconElement("Moderator", rankIconsLocation + "mod.png");
            }

            rankingColumnIcons.appendChild(eliteIcon);
            rankingColumnIcons.innerHTML += "&nbsp;";
            rankingColumnIcons.appendChild(rankIcon);
            rankingColumnIcons.innerHTML += "&nbsp;";
            rankingColumnIcons.appendChild(staffIcon);

            return rankingRow;
        }

        let rankings = <?php echo json_encode($rankings); ?>;
        var colours = ['#393939', '#303030'];
        var selected_colour = 0;

        rankings.forEach(ranking => {
            let tableElement = document.getElementById("ranking-table");
            let rankingRow = createRankingRowElement(ranking);
            rankingRow.bgcolor = selected_colour;
            tableElement.appendChild(rankingRow);
            selected_colour = selected_colour === 0 ? 1 : 0;
        });
    </script>
</body>
</html>