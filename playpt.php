<?php

/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

global $page_name;
global $page_location;
$page_name = "Pawn Tactics";
$page_location = "playpt.php";

require('forum/src/XF.php');

XF::start("/");
$app = XF::setupApp('XF\Pub\App');
$app->run();
$session = $app->session();
$sid = $session->getSessionId();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require ('src/elements/head.php') ?>

    <style>
        .gameFrame {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .gameSwf {
            width: 1250px;
            height: 939px;

            position = "fixed";
            top = 0;
            left = '12.5%';

        }
        .fullScreenToggle {
            position: fixed;
            right: 10px;
            top: 10px;
            z-index: 1001;
            cursor: pointer;
        }
    </style>
</head>

<body>
        <span class="gameFrame">
        <object class="gameSwf" type="application/x-shockwave-flash" data="https://d1h3njoxaty54c.cloudfront.net/pawn-tactics/experimental/ptmain.swf?sid=<?php echo $sid ?>">
            <param name="wmode" value="direct">
        </object>
        </span>
        <a onclick="enterFullScreen()" class="fullScreenToggle">Full Screen</a>

        <script>
            let fullscreenLinkElement = document.querySelector(".fullScreenToggle");
            let isFullScreen = false;
            let gameElement = document.querySelector(".gameSwf");

            function init() {
                resizeGame();
            }

            function resizeGame() {
                let gameWidthHeight = getGameWidthHeight(document.documentElement.clientWidth, document.documentElement.clientHeight);
                gameElement.style.height = gameWidthHeight.height + "px";
                gameElement.style.width = gameWidthHeight.width + "px";
            }

            function enterFullScreen() {
                if (gameElement.requestFullscreen)
                    gameElement.requestFullscreen();
                else if (gameElement.msRequestFullscreen)
                    gameElement.msRequestFullscreen();
                else if (gameElement.mozRequestFullScreen)
                    gameElement.mozRequestFullScreen();
                else if (gameElement.webkitRequestFullscreen)
                    gameElement.webkitRequestFullscreen();
            }

            function getGameWidthHeight(containerWidth, containerHeight) {
                let calculatedHeight = findNextDivisibleHeight(containerHeight);
                let calculatedWidth = calculateWidth(calculatedHeight);
                if (calculatedWidth > containerWidth) {
                    calculatedHeight = findNextDivisibleHeight(0.75 * containerWidth);
                    calculatedWidth = calculateWidth(calculatedHeight);
                }
                return {
                    width: calculatedWidth,
                    height: calculatedHeight
                };
            }

            function findNextDivisibleHeight(height) {
                while (height % 3 > 0) {
                    height--;
                }
                return height;
            }

            function calculateWidth(height) {
                return height + (height / 3)
            }

            window.onresize = resizeGame;
            gameElement.onfullscreenchange = resizeGame;

            init();
        </script>
</body>
</html>