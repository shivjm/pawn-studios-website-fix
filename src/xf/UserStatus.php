<?php


/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

namespace PawnStudios;
use XF\Entity\User;

class UserStatus
{
    public static function is_moderator(&$user) {
        if ($user instanceof User) {
            return $user->isMemberOf(34);
        }
        return false;
    }

    public static function is_admin(&$user) {
        if ($user instanceof User) {
            return $user->isMemberOf(3);
        }
        return false;
    }
}