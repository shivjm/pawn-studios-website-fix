<?php



/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

namespace PawnStudios;
use XF;

include('forum/src/XF.php');


class Finder
{


    /**
     * Finder constructor.
     */
    public function __construct()
    {
        XF::start('/');
    }

    public function findUserFromName($name) {
        $finder = XF::finder('XF:User');
        return $finder->where('username', $name)->fetchOne();
    }
}