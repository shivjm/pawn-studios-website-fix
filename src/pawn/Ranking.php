<?php


/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

namespace PawnStudios;
require_once ('src/pawn/Rank.php');

class Ranking {
    public $ranking;
    public $name;
    public $score;
    public $kills;
    public $deaths;
    public $kd;
    public $mvp;
    public $rank;

    /**
     * Ranking constructor.
     * @param $ranking
     * @param $name
     * @param $score
     * @param $kills
     * @param $deaths
     * @param $mvp
     * @param $rank
     */
    public function __construct($ranking, $name, $score, $kills, $deaths, $mvp)
    {
        $this->ranking = $ranking;
        $this->name = $name;
        $this->score = $score;
        $this->kills = $kills;
        $this->deaths = $deaths;
        $this->mvp = $mvp;
        $this->rank = new Rank($score);

        $this->kd = $deaths == 0 ? number_format((float)$kills, 2) : number_format((float)($kills / $deaths), 2);
    }
}