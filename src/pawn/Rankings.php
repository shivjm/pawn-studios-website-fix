<?php


/*
 * Copyright © 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

namespace PawnStudios;
use mysqli;

require_once('src/config.php');
require_once('src/pawn/Ranking.php');

class Rankings
{
    /**
     * @return Ranking[]
     */
    public static function getRankingSet($rankings_start, $rankings_end) {
        global $config;
        $ranking_set = array();

        $mysqli = new mysqli($config['db_host'], $config['pawn_rankings_user'], $config['pawn_rankings_pw'], $config['pawn_db']);
        $is_connected = $mysqli->connect_errno == 0;
        if ($is_connected) {
            $query_string = "SELECT RANK, `NAME`, SCORE, KILLS, DEATHS, MVP " .
                "FROM " . $config['rankings_table'] .
                " WHERE RANK > ? AND RANK <= ? ORDER BY RANK";
            $sql_statement = $mysqli->prepare($query_string);
            $sql_statement->bind_param('ii', $rankings_start, $rankings_end);
            $success = $sql_statement->execute();
            if ($success) {
                $result = $sql_statement->get_result();
                foreach ($result as $row) {
                    $ranking = new Ranking(
                        $row['RANK'],
                        $row['NAME'],
                        $row['SCORE'],
                        $row['KILLS'],
                        $row['DEATHS'],
                        $row['MVP']
                    );
                    $ranking_set[] = $ranking;
                }
            }
        } else {
            $ranking_set[] = new Ranking(1, "Failed to load rankings.", 0, 9001, 9001, -9001);
        }
        return $ranking_set;
    }
}

